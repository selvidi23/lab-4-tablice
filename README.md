# README #

Proszę napisać program z następującymi opcjami (menu na instrukcji switch):

Wprowadzanie liczb (maks. 100)  
Wyświetlanie liczb  
Ile liczb jest wprowadzonych  
Obliczanie sumy  
Obliczanie średniej  
Zmiana liczby  
Usunięcie liczby z końca  
Usunięcie dowolnej liczby  
Wyszukanie najmniejszej i największej liczby  
Wyświetlenie statystyki (ile parzystych, ile nieparzystych, ile dodatnich, ile ujemnych)  