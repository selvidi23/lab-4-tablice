#include <stdio.h>

// Proszę napisać program z następującymi opcjami (menu na instrukcji switch):
// Wprowadzanie liczb (maks. 100)
// Wyświetlanie liczb
// Ile liczb jest wprowadzonych
// Obliczanie sumy
// Obliczanie średniej
// Zmiana liczby
// Usunięcie liczby z końca
// Usunięcie dowolnej liczby
// Wyszukanie najmniejszej i największej liczby
// Wyświetlenie statystyki (ile parzystych, ile nieparzystych, ile dodatnich, ile ujemnych)
// Oceny: 4 na 3, pół oceny za każdy dodatkowy punkt

int tab[100];
int how_many_numbers;
int counter_numbers = 0;

void input_numbers(){
    int temp;
    for(int i=0;i<how_many_numbers; i++){
        scanf("%d",&tab[i]);
        counter_numbers++;
        temp = i;
    }
    for(int i = temp+1;i<100;i++ ){
        tab[i] = 0;
    }
}

void print_numbers(){
    for(int i=0;i<counter_numbers; i++){
        printf("%d, ", tab[i]);
    }
}

void print_how_many_numbers(){
    printf("Liczb wprowadzonych zostało - %d",counter_numbers);
}

int calc_sum(){
    int result = 0;
    for(int i = 0;i<how_many_numbers; i++){
        result = result + tab[i];
    }
    return result;
}

float calc_arithmetic_avg(){
    
    return (float)calc_sum()/counter_numbers;
}

int search_number_inedex(int searched_number){
    for (int i = 0; i<counter_numbers; i++){
        if(tab[i] == searched_number){
            return i;
        }
    }
    return 404;
}

void change_number_in_tab(int number_before, int number_after){
    
    if(search_number_inedex(number_before) == 404){
        printf("Nie ma takiej liczby w tablicy ");
    }
    else{
        tab[search_number_inedex(number_before)] = number_after;
    }

}

void remove_lastNumber(){
    tab[counter_numbers-1] =0;
    counter_numbers--;

}

void remove_any_number(int nubmer_to_remove){
    
    if(search_number_inedex(nubmer_to_remove) == 404){
        printf("Nie ma takiej liczby w tablicy ");
    }
    else{
        for(int i = search_number_inedex(nubmer_to_remove);i<counter_numbers; i++){
            tab[i] = tab[i+1];
        }
        tab[counter_numbers] = 0;
        counter_numbers--;
    }
}

int min(){
    int min =tab[0];
    for(int i=0;i<counter_numbers;i++){
        if(tab[i]<=min){
            min = tab[i];
        }
    }
    return min;
}

int max(){
    int max =tab[0];
    for(int i=0;i<counter_numbers;i++){
        if(tab[i]>=max){
            max = tab[i]; 
        }
    }
    return max;
}

void stats(){
    int even_numbers = 0, odd_numbers = 0, positive_numbers = 0, negative_numbers = 0;
    for(int i=0;i<counter_numbers;i++){
        
        if(tab[i] % 2 == 0){
            even_numbers++;
        }
        else{
            odd_numbers++;
        }

        if(tab[i] > 0){
            positive_numbers++;
        }else{
            negative_numbers++;
        }
    }
    printf("\n");
    printf("Parzystych liczb jest: %d \n",even_numbers);
    printf("Nieparzystych liczb jest: %d \n",odd_numbers);
    printf("Dodatnich liczb jest: %d \n",positive_numbers);
    printf("Ujemnych liczb jest: %d \n",negative_numbers);
}

int main(){

int select;
int number_before, number_after;
int number_to_remove;
    do{
        printf("\n");
        printf("Wybierz dzialanie\n\n");

        printf("1 - Wprowadzenie liczb max 100\n");
        printf("2 - Wyswietlenie liczb \n");
        printf("3 - Ile jest liczb wprowadzonych\n");
        printf("4 - Obliczanie sumy\n");
        printf("5 - Obliczanie średniej \n");
        printf("6 - Zmiana liczby   \n");
        printf("7 - Usunięcie Ostatniej liczby z tablicy  \n");
        printf("8 - Usuniecie dowolnej liczby \n");
        printf("9 - Min Max\n");
        printf("10 - Wyświetlenie statystyki (ile parzystych, ile nieparzystych, ile dodatnich, ile ujemnych) \n");

        printf("0 - Koniec\n\n");

        scanf("%d",&select);

        printf("\n");
        
        switch(select){
            case 1:
                printf("Podaj ile liczb w tablicy \n");
                scanf("%d", &how_many_numbers);
                printf("\n");
                input_numbers();
                break;
            case 2:
                print_numbers();
                break;
            case 3:
                print_how_many_numbers();
                break;
            case 4:
                printf("Suma Wynosi - %d",calc_sum());
                break;
            case 5:
                printf("Średnia arytmetyczna wynosi %f",calc_arithmetic_avg());
                break;
            case 6:
               
                print_numbers();
                printf("\n");
                printf("Podaj liczbe jaką chcesz zmienic \n");
                scanf("%d",&number_before);
                printf("Podaj liczbe na jaka chcesz zmienić \n");
                scanf("%d", &number_after);
                change_number_in_tab(number_before,number_after);
                printf("\n");
                printf("Twoje liczby po zmianie \n");
                print_numbers();
                break;
            case 7:
                print_numbers();
                printf("\n");
                remove_lastNumber();
                print_numbers();
                printf("\n");
                break;
            case 8:
                
                printf("\n");
                printf("Podaj cyfre do usuniecia ");
                scanf("%d", &number_to_remove);
                remove_any_number(number_to_remove);
                printf("\n");
                print_numbers();
                break;
            case 9:
                printf("\n");
                printf("Twoje Max to: %d \n Twoje Min to: %d",max(),min());
                break;
            case 10:
                stats();
                break;



        }

    }while(select !=0);

    return 0;
}